<?php

/**
 * @file tax_menu.module
 * @author Jennifer Lea Lampton <http://drupal.org/user/85586>
 */

/**
 * Implementation of hook_menu().
 */
function tax_menu_menu() {
  $items['admin/settings/tax_menu'] = array(
    'title' => t('Taxonomy Menus'),
    'description' => 'Allows the user to configure settings for  Taxonomy Menus.',
    'file' => 'tax_menu.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tax_menu_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  
  return $items;
}

/**
 * Implementation of hook_taxonomy().
 */
function tax_menu_taxonomy($op, $type, $array = NULL) {
  if ($type == 'term') {
    $menus = variable_get('tax_menu_vocabs', array());
    if (in_array($array['vid'], $menus)){
      switch($op) {
        case 'delete':
        case 'update':
        case 'insert':
          // We need to rebuild the menu, but don't want to do it more than necessary.
          tax_menu_rebuild_menu($array['vid'], $silent = TRUE);
          break;
      } 
    }
  }

}

/**
 * Rebuilds a Taxonomy Menu in the system
 *
 * @param $vid
 *   The vocabulary id for the menu to remove
 */
function tax_menu_rebuild_menu($vid, $silent = FALSE){
  if (!$vid){
    return;
  }
  
  tax_menu_remove_menu($vid, FALSE);
  tax_menu_build_menu($vid, FALSE, FALSE, FALSE);
  
  $vocab = taxonomy_vocabulary_load($vid);
  if (!$silent){
    drupal_set_message(t('Custom menu for !vocab has been rebuilt.', array('!vocab' => $vocab->name))); 
  }
  menu_cache_clear('tax_menu_'.$vid);
  _menu_clear_page_cache();
  
  return;
}

/**
 * Removes a Taxonomy Menu from the system
 *
 * @param $vid
 *   The vocabulary id for the menu to remove.
 * @param $message
 *   Indicates weather to set a message to the user, defaults to true.
 */
function tax_menu_remove_menu($vid, $message = TRUE){
  if (!$vid){
    return;
  }
  
  // Remove menu items and menu
  db_query("DELETE FROM {menu_links} WHERE menu_name = '%s'", 'tax_menu_'.$vid);
  db_query("DELETE FROM {menu_custom} WHERE menu_name = '%s'", 'tax_menu_'.$vid);
  
  if ($message){
    $vocab = taxonomy_vocabulary_load($vid);
    drupal_set_message(t('Custom menu for !vocab has been removed', array('!vocab' => $vocab->name)));
    // TODO this is not the right place for this cache clear
    menu_cache_clear('tax_menu_'.$vid);
    _menu_clear_page_cache();
  }
  
  return;
}

/**
 * Adds a Taxonomy Menu to the system
 *
 * @param $vid
 *   The vocabulary id for the menu to add
 * @param $hide_epmty
 *   Hide links to terms with no notes
 * @param $show_count
 *   Show counts next to term names
 * @param $message
 *   Indicates weather to set a message to the user, defaults to true.
 */
function tax_menu_build_menu($vid, $hide_epmty = FALSE, $show_count = FALSE, $message = TRUE){
  if (!$vid){
    return;
  }
  
  $vocab = taxonomy_vocabulary_load($vid);
  
  // create the menu for the vocab
  db_query("INSERT INTO {menu_custom} (menu_name, title, description) VALUES ('%s', '%s', '%s')", 'tax_menu_'.$vocab->vid, $vocab->name, $vocab->description);
  
  // create the menu items for the vocab
  $parents = array();
  $tree = taxonomy_get_tree($vid);
  foreach ($tree as $term) {
    $count = 0;
    // Need to count for count and empty
    if ($hide_empty || $show_count){
      // Calculate the numbers of children nodes
      $count = taxonomy_term_count_nodes($term->tid);
    }

    // if empty shown, or count > 0
    if (!($hide_empty && ($count > 0))) {
      $name = ($show_count) ? $term->name . ' (' . $count .')' : $term->name;
      $parent_menu = '';
      if ($term->depth > 0){
        $parent = array_pop($term->parents);
        $parent_menu = $parents[$parent];
      }
      $item = array(
        'link_title' => t($name),
        'link_path' => 'taxonomy/term/'.$term->tid,
        'menu_name' => 'tax_menu_'.$term->vid,
        'module' => 'tax_menu',
        'weight' => $term->weight,
        'plid' => $parent_menu,
      );
      $mlid = menu_link_save($item);
      
      // set the parent info for the next term
      
      $parents[$term->tid] = $mlid;
    }
  }
  
  if ($message){
    drupal_set_message(t('Custom menu for !vocab has been created', array('!vocab' => $vocab->name)));
    // TODO this is not the right place for this cache clear
    // menu_cache_clear('tax_menu_'.$vid);
    // _menu_clear_page_cache();
  }
  
  return;
}

/*
 * Implementation of hook_form_FORM_ID_alter().
 *
 * Since there is no hook for the re-order term page, we add
 * a Submit function instead.
 */
function tax_menu_form_taxonomy_overview_terms_alter(&$form, $form_state){
  $form['#submit'][] = 'tax_menu_taxonomy_overview_terms_submit';
}

/*
 * Submit function for taxonomy_overview_terms.
 *
 * Rebuild the menu on term re-order.
 */
function tax_menu_taxonomy_overview_terms_submit($form, &$form_state){
  drupal_set_message(t('Custom menu for !vocab has been rebuilt.', array('!vocab' => $vocab->name))); 
  //TODO: once istead of for each term? 
  //tax_menu_rebuild_menu($form['#vocabulary']['vid']); 
}