<?php

/**
 * @author Jennifer Lea Lampton <http://drupal.org/user/85586>
 * @file tax_menu.inc
 * Administration page
 */

/**
 * Choose which vocabularies get menus of their own
 * @return
 *   Array. The form fields.
 * @ingroup forms
 */
function tax_menu_admin_settings(&$form_state) {
  $form = array();
  $options = array();
  foreach (taxonomy_get_vocabularies() as $vocab) {
    $options[$vocab->vid] = $vocab->name;
  }
  $form['tax_menu_vocabs'] = array(
    '#type'           => 'checkboxes',
    '#title'          => t('Menus for Vocabularies'),
    '#options'        => $options,
    '#description'    => t('Each vocabulary checked will get a menu in the Drupal menu system.'),
    '#default_value'  => variable_get('tax_menu_vocabs', array()),
  );

  $form['#submit'][] = 'tax_menu_admin_settings_submit';
  
  return system_settings_form($form);
}

/**
 * Adds or removes Taxonomy Menus
 */
function tax_menu_admin_settings_submit($form, &$form_state) {
  // the following menu rebuild is expensive, let's only do it on change
  $menu_vocabs = variable_get('tax_menu_vocabs', array());
  
  // Save new custom menus for each vocab selected
  foreach ($form_state['values']['tax_menu_vocabs'] as $vid => $value) {
    if ((($value === $vid) && ($menu_vocabs[$vid] === 0 || empty($menu_vocabs)))){
      tax_menu_build_menu($vid);
    }
    else if (($value === 0) && ($menu_vocabs[$vid] === $vid)){
      tax_menu_remove_menu($vid);
    }
  }
}
